
package com.mycompany.week7;

import java.util.ArrayList;

public class UserService {
    private static ArrayList<User> userList = new ArrayList<>();
    static {
        userList.add(new User ("admin","password"));
        
    }

    public static boolean addUser(User user){
        userList.add(user);
        return true;
    }
        public static boolean addUser(String userName,String password){
        userList.add(new User (userName,password));
        return true;
    }
 
    public static boolean updateUser(int index,User user){
        userList.set(index, user);
        return true;
    }
 
    public static User getUser(int index){
        if(index>userList.size()-1){
            return null;
        }
        return userList.get(index);
    }

        public static ArrayList<User> getUsers(){
        return userList;
    }
        public static ArrayList<User> serchUserName(String serchText){
        ArrayList<User> list = new ArrayList<>();
        for(User user:userList){
            if(user.getUserName().startsWith(serchText)){
                list.add(user);
            }
        }
        return userList;
    }
        public static boolean delUser(int index){
            userList.remove(index);
            return true;
        }
        public static boolean delUser(User user){
            userList.remove(user);
            save();
            return true;
        }
        public static User login (String userName,String password){
            for (User user : userList){
                if(user.getUserName().equals(userName)&&user.getPassword().equals(password)){
                    return user;
                }
            }
            return null;
        }
        public static void save(){
            
        }
        public static void load(){
            
        }

    

  
        
     
}
